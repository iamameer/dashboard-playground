export * from './text'
export * from "./layout/account-settings"


import DealsChart from './home/deals-chart'
import UpcomingEvents from './home/upcoming-events'
import DashboardTotalCountCard from './home/total-count-card'

import UpcomingEventsSkeleton from './skeleton/upcoming-events'
import AccordionHeaderSkeleton from './skeleton/accordion-header'
import KanbanColumnSkeleton from './skeleton/kanban'
import ProjectCardSkeleton from './skeleton/project-card'
import LatestActivitiesSkeleton from './skeleton/latest-activities'

export { DealsChart, UpcomingEvents, DashboardTotalCountCard,
  UpcomingEventsSkeleton, AccordionHeaderSkeleton, 
  KanbanColumnSkeleton, ProjectCardSkeleton, LatestActivitiesSkeleton }