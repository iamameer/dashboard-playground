//** 1. request to GraphQL for reusability //#middleware
const customFetch = async (url: string, options: RequestInit) => {
  
  //purpose: add authorisation headers
  const accessToken = localStorage.getItem("access_token");
  const headers = options.headers as Record<string, string>;

  return await fetch(url, {
    ...options,
    headers: {
      ...headers,
      Authorization: headers?.Authorization || `Bearer ${accessToken}`,
        "Content-Type": "application/json",
        "Apollo-Require-Preflight": "true", //CORS
    }
  })
}

//* 2. handling GraphQL errors
import { GraphQLFormattedError } from "graphql";
type Error = {
  message: string,
  statusCode: string | number,
}

const getGraphQLErrors = (body: Record<"errors", GraphQLFormattedError[] | undefined>):
Error | null => {
  //if body doesnt exist
  if(!body){
    return {
      message: "Unknown Error",
      statusCode: "INTERNAL_SERVER_ERROR"
    }
  }

  //if body contains errors
  if("errors" in body){
    const errors = body?.errors;
    //join the errors in single string
    const messages = errors?.map((error) => error.message).join(", "); 
    const code = errors?.[0]?.extensions?.code;

    return {
      message: messages || JSON.stringify(errors),
      statusCode: code || 500
    }
  }

  //return null if anything else
  return null;
}

export const fetchWrapper = async (url: string, options: RequestInit) => {
  const response = await customFetch(url, options);

  //clone the response to re-use it
  const responseClone = response.clone(); //to process in multiple ways
  const body = await responseClone.json();

  const error = getGraphQLErrors(body);

  if(error){
    throw error;
  }

  return response;
}