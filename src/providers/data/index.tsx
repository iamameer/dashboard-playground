import graphqlDataProvider, 
  { 
    GraphQLClient,
    liveProvider as graphqlLiveProvider
  } from "@refinedev/nestjs-query";
import { fetchWrapper } from "./fetch-Wrapper";
import { createClient } from "graphql-ws";

export const API_BASE_URL = "https://api.crm.refine.dev"
export const API_URL = `${API_BASE_URL}/graphql`;
export const WS_URL = "wss://api.crm.refine.dev/graphql"; 

//GraphQL client to make request to the GraphPL API
//param1: API_URL
//param2: fetch
export const client = new GraphQLClient(API_URL,{
  //Callback for RequestInit (built-in interface)
  fetch: async (url: string, options: RequestInit) => {
    try {
      return fetchWrapper(url, options)
    } catch (error) {
      return Promise.reject(error as Error);
    }
  }
})

//websocket to listen to subscription of GraphQL API
//any changes happen, listen immediately
//refine: live-provider : when user add something, auto update without refresh
export const wsClient = 
  //checking if we have acces to the window // we're at the browser
  (typeof window !== "undefined")
  ? createClient({
    url: WS_URL,
    connectionParams: () => {
      const accessToken = localStorage.getItem("access_token");
      return {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      };
    }
  })
: undefined //return undefined if its not browser

//data provider to make request to the GraphQL API
export const dataProvider = graphqlDataProvider(client);

//create live provider to make subscription to the GraphQL API
export const liveProvider = wsClient ? graphqlLiveProvider(wsClient) : undefined;