import type { IResourceItem } from "@refinedev/core";

import {
  DashboardOutlined,
  ProjectOutlined,
  ShopOutlined,
} from "@ant-design/icons";

export const resources: IResourceItem[] = [
  {
    name: "dashboard",
    list: "/",
    meta: {
      label: "Dashboard",
      icon: <DashboardOutlined />,
    },
  },
  {
    name: "companies",
    list: "/companies",
    show: "/companies/:id",
    create: "/companies/new",
    edit: "/companies/edit/:id",
    meta: {
      label: "Companies",
      icon: <ShopOutlined />,
    },
  },
  {
    name: "tasks",
    list: "/tasks",
    create: "/tasks/new",
    edit: "/tasks/edit/:id",
    meta: {
      label: "Tasks",
      icon: <ProjectOutlined />,
    },
  },
];

//! [ Other Examples
  //   {
  //     name: "blog_posts",
  //     list: "/blog-posts",
  //     create: "/blog-posts/create",
  //     edit: "/blog-posts/edit/:id",
  //     show: "/blog-posts/show/:id",
  //     meta: {
  //       canDelete: true,
  //     },
  //   },
  //   {
  //     name: "categories",
  //     list: "/categories",
  //     create: "/categories/create",
  //     edit: "/categories/edit/:id",
  //     show: "/categories/show/:id",
  //     meta: {
  //       canDelete: true,
  //     },
  //   },
  // ]